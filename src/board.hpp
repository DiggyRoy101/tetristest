#pragma once
#include <ncurses.h>

class Board {
	public:
		Board() {
			construct(0, 0);
		}
		Board(int width, int height) {
			construct(width, height);
		}
		void refresh() {
			wclear(board_window);
			addBorder();
			wrefresh(board_window);
		}
		void initialize() {
			refresh();
		}
		chtype getInput() {
			return wgetch(board_window);
		}
	private:
		WINDOW* board_window;  // Ncurses window
		int width, height;

		// Construct the size of the ncurses window
		void construct(int width, int height) {
			int xMax, yMax;
			getmaxyx(stdscr, yMax, xMax);
			this->width = width;
			this->height = height;

			// Calculate rows and columns for the board
			int start_row = (yMax / 2) - (height / 2);
			int start_column = (xMax / 2) - (width / 2);
			board_window = newwin(height, width, start_row, start_column);

			// Allow use of arrow keys
			keypad(board_window, true);

			// Set board refresh rate
			wtimeout(board_window, 150);
		}
		void addBorder() {
			box(board_window, 0, 0);
		}
};
