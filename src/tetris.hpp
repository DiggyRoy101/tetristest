#pragma once
#include <ncurses.h>
#include "board.hpp"

class Tetris {

	public:
		Tetris(int dimension) {
			setWidthHeight(dimension);
			initialize();
		}
		void initialize() {
			board.initialize();
			game_over = false;
		}
		void processInput() {
		}
		void updateState() {
		}
		void redraw() {
		}
		bool isOver() {
			return game_over;
		}
	
	private:
		Board board;
		bool game_over;
		void setWidthHeight(int dimension) {
			int height = dimension;
			int width = dimension * 2.5;
			board = Board(width, height);
		}
};
