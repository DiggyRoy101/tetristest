#include <ncurses.h>
#include "src/tetris.hpp"

const int BOARD_DIM = 25;

int main() {
	// initializes the screen
	// sets up memory and clears the screen
	initscr();
	// refreshes the screen to match what is in memory
	refresh();
	// Do not show keypresses
	noecho();
	//curs_set(0);
	
	// Create Game Object with a Board
	Tetris game(BOARD_DIM);
	
	// Game Loop
	while (!game.isOver()) {
		game.processInput();
		game.updateState();
		game.redraw();
	}
	
	// deallocates memory and ends ncurses
	endwin();
	return 0;
}
