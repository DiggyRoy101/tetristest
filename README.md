# TetrisTest


# Description

This is a Tetris game written in C++ using the NCurses library as the front-end. It is a simple project to learn the basics of game development.  
*NOTE: This is a Low Priority Project since this project is used for learning C++ and the basics of game development.*


# Dependencies

- NCurses


# Prerequisites

You'll need some utilities beforehand in order to compile both this project along with PDCurses:

- MSYS2 MINGW-64 which provides
    - G++ (C++ Compiler)
    - GCC (C Compiler)
    - NCurses
- GNU Make


# Building

The build process differs between Linux and Windows. The instructions are laid out as follows.


## Linux

Unix-like OS's should have both GCC and G++. If these compilers are not pre-installed, you can install them through your distribution's package manager. The same goes for GNU Make. *NOTE: If you need superuser privileges, make sure to prepend the command with* `sudo`*.*


### For Arch-based Distributions using Pacman:

```
pacman -S gcc make ncurses
```


### For Debian-based Distributions using APT:

```
apt install gcc make ncurses
```


### For Fedora using DNF:

```
dnf install gcc make ncurses
```

You might also need to install the `ncurses-devel` package if you get compilation errors. Some distros like Arch only require the base ncurses package to compile a ncurses application.
  

## Windows

On windows, in order to get a hold of GCC and G++, you need to install MSYS2 which comes with mingw-64. You also need GNU Make, which you can get through Chocolately.


### Installaing MSYS2

1. Head over to [MSYS2 homepage](https://www.msys2.org/) Download the installer and run it.
2. The installer will ask where you want to install it. You can keep the default which is *C:\msys64*.
3. You can click through *Next*, accepting the default options.


### Install Developer Utilities like GCC/G++ and NCurses

We need some developer tools to be able to compile our code. Open up MSYS2. You can search it up using the Start Menu. A console should appear. In that console, run the command:

```
pacman -S mingw-w64-x86_64-toolchain
```

Just hit ENTER when it asks which option to choose. Hitting ENTER will default to ALL.

Afterwards, run this command:

```
pacman -S ncurses ncurses-devel
```


### Setting up Environment Variables

1. Open up the file explorer and go to your **C** drive. Go to wherever msys64 is installed and inside it go to *mingw64* and then *bin*. Copy the file path. We'll need this later.
2. Open up the start menu and search for **Environment Variables**. After you open the corresponding appplication, click on the **Environment Variables** button.
3. You will get a list of environment variables on your system. The one we are interested is called **Path**. Select that entry and click the **Edit** button.
4. In the "Edit environment variable" window, click **New**.
5. Paste the path you copied earlier. If you haven't copied it earlier, the default path should be `C:\msys64\mingw64\bin`.
6. Click **OK** to save and exit. Click **OK** for all the other menus.

You should be able to call G++ from the terminal now without specifying its full path.


### Installing Make through Chocolately

If you don't have Chocolately enabled:

1. Open Powershell as an Administrator
2. Run the following command and wait for it to finish installing. Follow any prompts presented.

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

You should have chocolately on your system now. To install GNU Make, run the following command in Powershell as Administrator:  

```
choco install make
```

The original documentation for installing Chocolately is found [here](https://chocolatey.org/install).  


After restarting your console, you should now have the GNU C/C++ compiler and GNU Make with your environment variables correctly set up. You can test it out by running the commands `g++` and `make` in your terminal. If everything was setup correctly, you shouldn't have a *Command not found* or similar message.


### Change Default Home directory for MSYS2 (Optional)

Launch MSYS2. You can do so using the Start Menu. Run `nano /etc/fstab` and append this line `C:/Users /home ntfs binary,posix=0,noacl,user 0 0` to the end of the file. Save the file and relaunch MSYS2. This will make it so that everytime you launch MSYS, you will be in your Windows Home folder. 


## Compiling

You can just run `make` on the command line.


# Running Tetris


## For Linux

Run `./main`


## For Windows

Run `./main.exe`
