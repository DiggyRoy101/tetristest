
ifeq ($(OS),Windows_NT)
	BINARY = main.exe
	INCLUDE = -Ic:/msys64/mingw64/include/ncurses
	RM = del -fR
else
	BINARY = main
	INCLUDE =
	RM = rm -f
endif


main: main.cpp
	g++ $(INCLUDE) main.cpp -o $(BINARY) -lncurses -DNCURSES_STATIC
clean:
	$(RM) main main.exe
